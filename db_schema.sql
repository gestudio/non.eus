# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: h3.gestudio.com (MySQL 5.1.69-log)
# Database: admin_ipx
# Generation Time: 2014-03-19 14:03:02 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table countries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id` char(2) NOT NULL DEFAULT '',
  `name` varchar(80) NOT NULL,
  `printable_name` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `query_count` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `iso` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;

INSERT INTO `countries` (`id`, `name`, `printable_name`, `iso3`, `numcode`, `query_count`)
VALUES
	('AF','AFGHANISTAN','Afghanistan','AFG',4,1),
	('AL','ALBANIA','Albania','ALB',8,4),
	('DZ','ALGERIA','Algeria','DZA',12,20),
	('AS','AMERICAN SAMOA','American Samoa','ASM',16,0),
	('AD','ANDORRA','Andorra','AND',20,2),
	('AO','ANGOLA','Angola','AGO',24,3),
	('AI','ANGUILLA','Anguilla','AIA',660,0),
	('AQ','ANTARCTICA','Antarctica',NULL,NULL,0),
	('AG','ANTIGUA AND BARBUDA','Antigua and Barbuda','ATG',28,1),
	('AR','ARGENTINA','Argentina','ARG',32,384),
	('AM','ARMENIA','Armenia','ARM',51,10),
	('AW','ARUBA','Aruba','ABW',533,0),
	('AU','AUSTRALIA','Australia','AUS',36,16),
	('AT','AUSTRIA','Austria','AUT',40,37),
	('AZ','AZERBAIJAN','Azerbaijan','AZE',31,13),
	('BS','BAHAMAS','Bahamas','BHS',44,2),
	('BH','BAHRAIN','Bahrain','BHR',48,1),
	('BD','BANGLADESH','Bangladesh','BGD',50,3),
	('BB','BARBADOS','Barbados','BRB',52,0),
	('BY','BELARUS','Belarus','BLR',112,41),
	('BE','BELGIUM','Belgium','BEL',56,30),
	('BZ','BELIZE','Belize','BLZ',84,2),
	('BJ','BENIN','Benin','BEN',204,6),
	('BM','BERMUDA','Bermuda','BMU',60,0),
	('BT','BHUTAN','Bhutan','BTN',64,0),
	('BO','BOLIVIA','Bolivia','BOL',68,16),
	('BA','BOSNIA AND HERZEGOVINA','Bosnia and Herzegovina','BIH',70,3),
	('BW','BOTSWANA','Botswana','BWA',72,1),
	('BV','BOUVET ISLAND','Bouvet Island',NULL,NULL,0),
	('BR','BRAZIL','Brazil','BRA',76,188),
	('IO','BRITISH INDIAN OCEAN TERRITORY','British Indian Ocean Territory',NULL,NULL,0),
	('BN','BRUNEI DARUSSALAM','Brunei Darussalam','BRN',96,2),
	('BG','BULGARIA','Bulgaria','BGR',100,14),
	('BF','BURKINA FASO','Burkina Faso','BFA',854,2),
	('BI','BURUNDI','Burundi','BDI',108,0),
	('KH','CAMBODIA','Cambodia','KHM',116,0),
	('CM','CAMEROON','Cameroon','CMR',120,2),
	('CA','CANADA','Canada','CAN',124,66),
	('CV','CAPE VERDE','Cape Verde','CPV',132,0),
	('KY','CAYMAN ISLANDS','Cayman Islands','CYM',136,0),
	('CF','CENTRAL AFRICAN REPUBLIC','Central African Republic','CAF',140,0),
	('TD','CHAD','Chad','TCD',148,0),
	('CL','CHILE','Chile','CHL',152,200),
	('CN','CHINA','China','CHN',156,903),
	('CX','CHRISTMAS ISLAND','Christmas Island',NULL,NULL,0),
	('CC','COCOS (KEELING) ISLANDS','Cocos (Keeling) Islands',NULL,NULL,0),
	('CO','COLOMBIA','Colombia','COL',170,250),
	('KM','COMOROS','Comoros','COM',174,0),
	('CG','CONGO','Congo','COG',178,0),
	('CD','CONGO, THE DEMOCRATIC REPUBLIC OF THE','Congo, the Democratic Republic of the','COD',180,0),
	('CK','COOK ISLANDS','Cook Islands','COK',184,0),
	('CR','COSTA RICA','Costa Rica','CRI',188,32),
	('CI','COTE D\'IVOIRE','Cote D\'Ivoire','CIV',384,2),
	('HR','CROATIA','Croatia','HRV',191,5),
	('CU','CUBA','Cuba','CUB',192,11),
	('CY','CYPRUS','Cyprus','CYP',196,3),
	('CZ','CZECH REPUBLIC','Czech Republic','CZE',203,39),
	('DK','DENMARK','Denmark','DNK',208,7),
	('DJ','DJIBOUTI','Djibouti','DJI',262,0),
	('DM','DOMINICA','Dominica','DMA',212,0),
	('DO','DOMINICAN REPUBLIC','Dominican Republic','DOM',214,41),
	('EC','ECUADOR','Ecuador','ECU',218,79),
	('EG','EGYPT','Egypt','EGY',818,33),
	('SV','EL SALVADOR','El Salvador','SLV',222,12),
	('GQ','EQUATORIAL GUINEA','Equatorial Guinea','GNQ',226,0),
	('ER','ERITREA','Eritrea','ERI',232,0),
	('EE','ESTONIA','Estonia','EST',233,6),
	('ET','ETHIOPIA','Ethiopia','ETH',231,4),
	('FK','FALKLAND ISLANDS (MALVINAS)','Falkland Islands (Malvinas)','FLK',238,0),
	('FO','FAROE ISLANDS','Faroe Islands','FRO',234,0),
	('FJ','FIJI','Fiji','FJI',242,0),
	('FI','FINLAND','Finland','FIN',246,12),
	('FR','FRANCE','France','FRA',250,542),
	('GF','FRENCH GUIANA','French Guiana','GUF',254,0),
	('PF','FRENCH POLYNESIA','French Polynesia','PYF',258,0),
	('TF','FRENCH SOUTHERN TERRITORIES','French Southern Territories',NULL,NULL,0),
	('GA','GABON','Gabon','GAB',266,2),
	('GM','GAMBIA','Gambia','GMB',270,0),
	('GE','GEORGIA','Georgia','GEO',268,10),
	('DE','GERMANY','Germany','DEU',276,679),
	('GH','GHANA','Ghana','GHA',288,4),
	('GI','GIBRALTAR','Gibraltar','GIB',292,0),
	('GR','GREECE','Greece','GRC',300,7),
	('GL','GREENLAND','Greenland','GRL',304,0),
	('GD','GRENADA','Grenada','GRD',308,0),
	('GP','GUADELOUPE','Guadeloupe','GLP',312,0),
	('GU','GUAM','Guam','GUM',316,0),
	('GT','GUATEMALA','Guatemala','GTM',320,29),
	('GN','GUINEA','Guinea','GIN',324,0),
	('GW','GUINEA-BISSAU','Guinea-Bissau','GNB',624,0),
	('GY','GUYANA','Guyana','GUY',328,0),
	('HT','HAITI','Haiti','HTI',332,0),
	('HM','HEARD ISLAND AND MCDONALD ISLANDS','Heard Island and Mcdonald Islands',NULL,NULL,0),
	('VA','HOLY SEE (VATICAN CITY STATE)','Holy See (Vatican City State)','VAT',336,0),
	('HN','HONDURAS','Honduras','HND',340,5),
	('HK','HONG KONG','Hong Kong','HKG',344,15),
	('HU','HUNGARY','Hungary','HUN',348,26),
	('IS','ICELAND','Iceland','ISL',352,3),
	('IN','INDIA','India','IND',356,40),
	('ID','INDONESIA','Indonesia','IDN',360,53),
	('IR','IRAN, ISLAMIC REPUBLIC OF','Iran, Islamic Republic of','IRN',364,45),
	('IQ','IRAQ','Iraq','IRQ',368,6),
	('IE','IRELAND','Ireland','IRL',372,23),
	('IL','ISRAEL','Israel','ISR',376,19),
	('IT','ITALY','Italy','ITA',380,70),
	('JM','JAMAICA','Jamaica','JAM',388,1),
	('JP','JAPAN','Japan','JPN',392,76),
	('JO','JORDAN','Jordan','JOR',400,3),
	('KZ','KAZAKHSTAN','Kazakhstan','KAZ',398,21),
	('KE','KENYA','Kenya','KEN',404,1),
	('KI','KIRIBATI','Kiribati','KIR',296,0),
	('KP','KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF','Korea, Democratic People\'s Republic of','PRK',408,0),
	('KR','KOREA, REPUBLIC OF','Korea, Republic of','KOR',410,26),
	('KW','KUWAIT','Kuwait','KWT',414,1),
	('KG','KYRGYZSTAN','Kyrgyzstan','KGZ',417,1),
	('LA','LAO PEOPLE\'S DEMOCRATIC REPUBLIC','Lao People\'s Democratic Republic','LAO',418,0),
	('LV','LATVIA','Latvia','LVA',428,17),
	('LB','LEBANON','Lebanon','LBN',422,7),
	('LS','LESOTHO','Lesotho','LSO',426,0),
	('LR','LIBERIA','Liberia','LBR',430,1),
	('LY','LIBYAN ARAB JAMAHIRIYA','Libyan Arab Jamahiriya','LBY',434,2),
	('LI','LIECHTENSTEIN','Liechtenstein','LIE',438,1),
	('LT','LITHUANIA','Lithuania','LTU',440,11),
	('LU','LUXEMBOURG','Luxembourg','LUX',442,17),
	('MO','MACAO','Macao','MAC',446,0),
	('MK','MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF','Macedonia, the Former Yugoslav Republic of','MKD',807,4),
	('MG','MADAGASCAR','Madagascar','MDG',450,2),
	('MW','MALAWI','Malawi','MWI',454,0),
	('MY','MALAYSIA','Malaysia','MYS',458,12),
	('MV','MALDIVES','Maldives','MDV',462,0),
	('ML','MALI','Mali','MLI',466,0),
	('MT','MALTA','Malta','MLT',470,0),
	('MH','MARSHALL ISLANDS','Marshall Islands','MHL',584,0),
	('MQ','MARTINIQUE','Martinique','MTQ',474,1),
	('MR','MAURITANIA','Mauritania','MRT',478,0),
	('MU','MAURITIUS','Mauritius','MUS',480,0),
	('YT','MAYOTTE','Mayotte',NULL,NULL,0),
	('MX','MEXICO','Mexico','MEX',484,387),
	('FM','MICRONESIA, FEDERATED STATES OF','Micronesia, Federated States of','FSM',583,0),
	('MD','MOLDOVA, REPUBLIC OF','Moldova, Republic of','MDA',498,15),
	('MC','MONACO','Monaco','MCO',492,1),
	('MN','MONGOLIA','Mongolia','MNG',496,2),
	('MS','MONTSERRAT','Montserrat','MSR',500,0),
	('MA','MOROCCO','Morocco','MAR',504,24),
	('MZ','MOZAMBIQUE','Mozambique','MOZ',508,0),
	('MM','MYANMAR','Myanmar','MMR',104,9),
	('NA','NAMIBIA','Namibia','NAM',516,0),
	('NR','NAURU','Nauru','NRU',520,0),
	('NP','NEPAL','Nepal','NPL',524,0),
	('NL','NETHERLANDS','Netherlands','NLD',528,150),
	('AN','NETHERLANDS ANTILLES','Netherlands Antilles','ANT',530,0),
	('NC','NEW CALEDONIA','New Caledonia','NCL',540,2),
	('NZ','NEW ZEALAND','New Zealand','NZL',554,15),
	('NI','NICARAGUA','Nicaragua','NIC',558,22),
	('NE','NIGER','Niger','NER',562,3),
	('NG','NIGERIA','Nigeria','NGA',566,24),
	('NU','NIUE','Niue','NIU',570,0),
	('NF','NORFOLK ISLAND','Norfolk Island','NFK',574,0),
	('MP','NORTHERN MARIANA ISLANDS','Northern Mariana Islands','MNP',580,0),
	('NO','NORWAY','Norway','NOR',578,23),
	('OM','OMAN','Oman','OMN',512,2),
	('PK','PAKISTAN','Pakistan','PAK',586,20),
	('PW','PALAU','Palau','PLW',585,0),
	('PS','PALESTINIAN TERRITORY, OCCUPIED','Palestinian Territory, Occupied',NULL,NULL,9),
	('PA','PANAMA','Panama','PAN',591,13),
	('PG','PAPUA NEW GUINEA','Papua New Guinea','PNG',598,0),
	('PY','PARAGUAY','Paraguay','PRY',600,18),
	('PE','PERU','Peru','PER',604,221),
	('PH','PHILIPPINES','Philippines','PHL',608,9),
	('PN','PITCAIRN','Pitcairn','PCN',612,0),
	('PL','POLAND','Poland','POL',616,66),
	('PT','PORTUGAL','Portugal','PRT',620,35),
	('PR','PUERTO RICO','Puerto Rico','PRI',630,13),
	('QA','QATAR','Qatar','QAT',634,1),
	('RE','REUNION','Reunion','REU',638,1),
	('RO','ROMANIA','Romania','ROM',642,177),
	('RU','RUSSIAN FEDERATION','Russian Federation','RUS',643,822),
	('RW','RWANDA','Rwanda','RWA',646,0),
	('SH','SAINT HELENA','Saint Helena','SHN',654,0),
	('KN','SAINT KITTS AND NEVIS','Saint Kitts and Nevis','KNA',659,0),
	('LC','SAINT LUCIA','Saint Lucia','LCA',662,0),
	('PM','SAINT PIERRE AND MIQUELON','Saint Pierre and Miquelon','SPM',666,0),
	('VC','SAINT VINCENT AND THE GRENADINES','Saint Vincent and the Grenadines','VCT',670,0),
	('WS','SAMOA','Samoa','WSM',882,0),
	('SM','SAN MARINO','San Marino','SMR',674,0),
	('ST','SAO TOME AND PRINCIPE','Sao Tome and Principe','STP',678,0),
	('SA','SAUDI ARABIA','Saudi Arabia','SAU',682,29),
	('SN','SENEGAL','Senegal','SEN',686,2),
	('CS','SERBIA AND MONTENEGRO','Serbia and Montenegro',NULL,NULL,0),
	('SC','SEYCHELLES','Seychelles','SYC',690,0),
	('SL','SIERRA LEONE','Sierra Leone','SLE',694,2),
	('SG','SINGAPORE','Singapore','SGP',702,14),
	('SK','SLOVAKIA','Slovakia','SVK',703,4),
	('SI','SLOVENIA','Slovenia','SVN',705,8),
	('SB','SOLOMON ISLANDS','Solomon Islands','SLB',90,0),
	('SO','SOMALIA','Somalia','SOM',706,0),
	('ZA','SOUTH AFRICA','South Africa','ZAF',710,8),
	('GS','SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS','South Georgia and the South Sandwich Islands',NULL,NULL,0),
	('ES','SPAIN','Spain','ESP',724,1142),
	('LK','SRI LANKA','Sri Lanka','LKA',144,4),
	('SD','SUDAN','Sudan','SDN',736,1),
	('SR','SURINAME','Suriname','SUR',740,1),
	('SJ','SVALBARD AND JAN MAYEN','Svalbard and Jan Mayen','SJM',744,0),
	('SZ','SWAZILAND','Swaziland','SWZ',748,0),
	('SE','SWEDEN','Sweden','SWE',752,50),
	('CH','SWITZERLAND','Switzerland','CHE',756,44),
	('SY','SYRIAN ARAB REPUBLIC','Syrian Arab Republic','SYR',760,11),
	('TW','TAIWAN, PROVINCE OF CHINA','Taiwan, Province of China','TWN',158,26),
	('TJ','TAJIKISTAN','Tajikistan','TJK',762,2),
	('TZ','TANZANIA, UNITED REPUBLIC OF','Tanzania, United Republic of','TZA',834,0),
	('TH','THAILAND','Thailand','THA',764,17),
	('TL','TIMOR-LESTE','Timor-Leste',NULL,NULL,1),
	('TG','TOGO','Togo','TGO',768,0),
	('TK','TOKELAU','Tokelau','TKL',772,0),
	('TO','TONGA','Tonga','TON',776,0),
	('TT','TRINIDAD AND TOBAGO','Trinidad and Tobago','TTO',780,0),
	('TN','TUNISIA','Tunisia','TUN',788,21),
	('TR','TURKEY','Turkey','TUR',792,31),
	('TM','TURKMENISTAN','Turkmenistan','TKM',795,0),
	('TC','TURKS AND CAICOS ISLANDS','Turks and Caicos Islands','TCA',796,0),
	('TV','TUVALU','Tuvalu','TUV',798,0),
	('UG','UGANDA','Uganda','UGA',800,1),
	('UA','UKRAINE','Ukraine','UKR',804,536),
	('AE','UNITED ARAB EMIRATES','United Arab Emirates','ARE',784,5),
	('GB','UNITED KINGDOM','United Kingdom','GBR',826,133),
	('US','UNITED STATES','United States','USA',840,2349),
	('UM','UNITED STATES MINOR OUTLYING ISLANDS','United States Minor Outlying Islands',NULL,NULL,0),
	('UY','URUGUAY','Uruguay','URY',858,27),
	('UZ','UZBEKISTAN','Uzbekistan','UZB',860,12),
	('VU','VANUATU','Vanuatu','VUT',548,0),
	('VE','VENEZUELA','Venezuela','VEN',862,119),
	('VN','VIET NAM','Viet Nam','VNM',704,14),
	('VG','VIRGIN ISLANDS, BRITISH','Virgin Islands, British','VGB',92,4),
	('VI','VIRGIN ISLANDS, U.S.','Virgin Islands, U.s.','VIR',850,0),
	('WF','WALLIS AND FUTUNA','Wallis and Futuna','WLF',876,0),
	('EH','WESTERN SAHARA','Western Sahara','ESH',732,0),
	('YE','YEMEN','Yemen','YEM',887,6),
	('ZM','ZAMBIA','Zambia','ZMB',894,0),
	('ZW','ZIMBABWE','Zimbabwe','ZWE',716,0);

/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table queries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `queries`;

CREATE TABLE `queries` (
  `id` char(36) NOT NULL DEFAULT '',
  `ip` varchar(15) DEFAULT NULL,
  `query` varchar(64) NOT NULL,
  `country` varchar(3) DEFAULT NULL,
  `reverse` varchar(64) DEFAULT NULL,
  `geodata` longtext,
  `query_request_count` int(11) NOT NULL DEFAULT '0',
  `created` int(11) unsigned NOT NULL,
  `modified` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `query` (`query`),
  KEY `country` (`country`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table query_blacklists
# ------------------------------------------------------------

DROP TABLE IF EXISTS `query_blacklists`;

CREATE TABLE `query_blacklists` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `query` varchar(64) NOT NULL DEFAULT '',
  `response` longtext,
  `created` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `query_id` (`query`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table query_comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `query_comments`;

CREATE TABLE `query_comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `query_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `query_id` (`query_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table query_requests
# ------------------------------------------------------------

DROP TABLE IF EXISTS `query_requests`;

CREATE TABLE `query_requests` (
  `id` int(15) unsigned NOT NULL AUTO_INCREMENT,
  `query_id` char(36) NOT NULL DEFAULT '',
  `request_ip` varchar(15) NOT NULL DEFAULT '',
  `created` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `query_id` (`query_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table query_servers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `query_servers`;

CREATE TABLE `query_servers` (
  `id` char(36) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `query` varchar(64) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `response` longtext CHARACTER SET latin1,
  `created` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `query` (`query`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table whois
# ------------------------------------------------------------

DROP TABLE IF EXISTS `whois`;

CREATE TABLE `whois` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `query` varchar(64) NOT NULL DEFAULT '',
  `value` longtext NOT NULL,
  `server` varchar(32) DEFAULT NULL,
  `created` int(11) unsigned NOT NULL,
  `modified` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
