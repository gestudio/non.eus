
<section style="margin-top:10px">
	<div class="container">


		<div class="row">
			<h2><?php echo $country['Country']['printable_name']; ?></h2>
		</div>



		<div class="row">


			<table class="table table-stripped">
				<thead>
					<tr>
						<th><?php echo $this->Paginator->sort('query_request_count',__('Requests'));?></th>
						<th><?php echo $this->Paginator->sort('query',__('Query'));?></th>
						<th><?php echo $this->Paginator->sort('country',__('Country'));?></th>
						<th><?php echo __('City'); ?></th>
						<th><?php echo __('Region'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($rows as $row): ?>
						<tr>
							<td><?php echo $row['Query']['query_request_count']; ?></td>
							<td><?php echo $this->Html->link($row['Query']['query'],'/' . $row['Query']['query']); ?> <small class="text-muted"><?php echo $row['Query']['reverse']; ?></small></td>
							<td><?php echo $this->Html->link($row['Query']['country'],'/main/country/'.$row['Query']['country']); ?></td>
							<td><?php if(isset($row['Query']['geodata']['city'])) echo $row['Query']['geodata']['city']; ?></td>
							<td><?php if(isset($row['Query']['geodata']['region'])) echo $row['Query']['geodata']['region']; ?></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>



			<?php echo $this->Element('paging'); ?>


		</div>

	</div>
</section>
