<?php if(strpos($_SERVER['SERVER_NAME'], 'nomap') === false):?>
  <!--Header-->
    <section id="header">
        <div class="row" id="map-canvas"></div>
    </section>
    <?php else: ?>
    <div id="headerNoMap">
    	No Map version
    </div>
	<?php endif; ?>
 <!--Main-->
    <section id="main" class="white-bg">
      <div class="container">
        <div class="row text-center">
          
          <!--Section-->
          <div class="col-sm-12 <?php if(empty($rec['Domain'])):?>col-md-6<?php else:?>col-md-4<?php endif;?> margin-30 animated fadeIn delay1">
          	<div class="panel panel-default">
			  <div class="panel-heading">
				<h3 class="panel-title" title="Updated <?php echo $this->Time->timeAgoInWords($rec['Query']['modified']);?>"><i class="fa fa-location-arrow"></i> <?php echo __d('layout','Geolocation'); ?> <small><?php echo number_format($geoTime,2);?> s.</small></h3>
			  </div>
			  <div class="panel-body">
			     <dl class="dl-horizontal">
			     	
			     	<?php if(array_key_exists('city', $rec['Query']['geodata'])): ?>
					<dt><?php echo __d('layout','City'); ?></dt>
					<dd><?php echo $rec['Query']['geodata']['city'] ?></dd>
					<?php endif; ?>
					
					<?php if(array_key_exists('region', $rec['Query']['geodata'])): ?>
					<dt><?php echo __d('layout','Region'); ?></dt>
					<dd><?php echo $rec['Query']['geodata']['region'] ?></dd>
					<?php endif;?>
					
					<dt><?php echo __d('layout','Country'); ?></dt>
					<dd><?php echo $this->Html->image('flags/'.strtolower($rec['Query']['geodata']['countryCode']).'.png');?> (<?= $rec['Query']['geodata']['countryCode'] ?>) <?= $rec['Query']['geodata']['country'] ?></dd>
				</dl>
			  </div>
			</div>
          </div>
          
          
          <!--Section-->
          <div class="col-sm-12 <?php if(empty($rec['Domain'])):?>col-md-6<?php else:?>col-md-4<?php endif;?> margin-30 animated fadeIn delay1">
          	<div class="panel panel-default">
			  <div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-sitemap"></i> <?php echo  __d('layout','Network'); ?></h3>
			  </div>
			  <div class="panel-body">
			     <dl class="dl-horizontal">
					<dt><?php echo __d('layout','IP address'); ?></dt>
					<dd><?php echo $rec['Query']['ip'] ?></dd>
					<dt><?php echo __d('layout','Reverse'); ?></dt>
					<dd><?php echo $rec['Query']['reverse'] ?></dd>
					<dt><?php echo __d('layout','ISP'); ?></dt>
					<dd><?php echo $rec['Query']['geodata']['isp']; ?></dd>
					
					<?php 
					if(isset($rec['QueryBlacklist']) and isset($rec['QueryBlacklist']['response'])):
					foreach($rec['QueryBlacklist']['response'] as $server => $status): ?>
						<dt><?php echo $server; ?></dt>
						<dd><?php 
							if($status)
								echo '<span class="text-danger"><i class="fa fa-times-cicrcle"></i> '.__d('layout','BANNED').'</span>';
							else 
								echo '<span class="text-success"><i class="fa fa-check-circle"></i> '.__d('layout','OK!').'</span>'; 
						?>
						</dd>
					<?php endforeach;endif; ?>
					
				</dl>
				
			  </div>
			</div>
          </div>
          
          
          <?php if(!empty($rec['Domain'])):?>
          <!--Section-->
          <div class="col-sm-12 col-md-4 animated fadeIn delay1">
          	<div class="panel panel-default">
			  <div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-flask"></i> <?php echo __d('layout','Domain and server information'); ?> <small><?php echo number_format($wwwTime,2);?> s.</small></h3>
			  </div>
			  <div class="panel-body">
			     <dl class="dl-horizontal" id="serverInfo">
			     	<?php 
			     	if (!empty($rec['QueryServer'])):
				     	foreach ($rec['QueryServer']['response'] as $key => $value):
					     	
					     	if($key=='Set-Cookie')continue;
					     	if($key=='Date')continue;
					     	
					     	echo $this->Html->tag('dt',$key);
					     	echo $this->Html->tag('dd',$this->Html->tag('small',$value));
							
						endforeach;
					endif;
					?>
				</dl>
			  </div>
			</div>
          </div>
          <!--Section-->
          <?php endif;?>
          
          
        </div>
      </div>
	  <div class="container">
			<div style="row">
				<?php echo $this->Form->postLink('<i class="fa fa-refresh"></i> '.__('Cached on').' '.$this->Time->timeAgoInWords($rec['Query']['modified']),$this->here.'?q='.$rec['Query']['query'],array('class' => 'btn btn-info col-md-3 col-md-offset-2','escapeTitle' => false));?>
			
				<a class="btn btn-danger col-md-3 col-md-offset-1" href="https://www.xvbelink.com/?a_fid=gestudio"><i class="fa fa-user-secret"></i> Hide your IP address with a VPN</a>
			</div>
		</div>
    </section>


	<section id="main2" class="gray-bg">
      <div class="container">
        <div class="row">
          
          <?php if(empty($rec['Domain'])):?>
          <div class="col-sm-9 margin-30">
            <pre id="ipWhoisData"><?php if(isset($rec['Whois'])) echo $rec['Whois']['value']; else echo '<i style="color:#ccc;" class="fa fa-spinner fa-spin fa-5x"></i>'; ?></pre>
          </div>
          
          <div class="col-sm-3 text-right">
            <h3><?php echo __d('layout','The Whois Record');?></h3>
            <p><?php echo __d('layout','This is the Whois data gathered from the registrant servers.');?></p>
          </div>
          <?php else:?>
          <div class="col-sm-4 margin-30">
            <pre id="ipWhoisData"><?php if(isset($rec['Whois'])) echo $rec['Whois']['value']; else echo '<i style="color:#ccc;" class="fa fa-spinner fa-spin fa-5x"></i>'; ?></pre>
          </div>
          <div class="col-sm-8 margin-30">
            <pre id="domainWhoisData"><?php if(!empty($rec['Domain']) && isset($rec['Domain']['whois'])) echo $rec['Domain']['whois']['value']; else echo '<i style="color:#ccc;" class="fa fa-spinner fa-spin fa-5x"></i>'; ?></pre>
          </div>
          <?php endif;?>
          
          
        </div>
      </div>
    </section>
	
	
	
	
	
	<section class="color-bg">
	  <div class="container">
	    <div class="row">
	      <div class="col-sm-12 margin-40 text-center">
	        <h2 class="white"><?php 
	        App::uses('CakeNumber', 'Utility');

	        echo CakeNumber::format($totalRequests, array( 'before' => NULL, 'places' => 0, 'escape' => false, 'thousands' => '.')).' '. __d('layout','requests processed!'); ?></h2>
	      </div>
	    </div>
	    
	    <div id="ajaxPagination" class="text-center"><i style="color:white;" class="fa fa-spinner fa-spin fa-5x"></i></div>
	    
	    
	  </div>
	</section>
	


<script type="text/javascript">
//<![CDATA[
	
	
	$( document ).ready(function() {
		
		
		
		function checkAjaxPagingLinks(){
			$('ul.pagination a').bind('click',function(e){
				
				e.preventDefault();
				
				if($(this).attr('href') != undefined){
					$('#ajaxPagination').fadeTo('fast',0.1);
					
					$.ajax({
						url: $(this).attr('href')
					}).done(function( data ){
						$('#ajaxPagination').removeClass('text-center').html(data);
						$('#ajaxPagination').fadeTo('fast',1);
						checkAjaxPagingLinks();	
					}).fail(function(){
						$('#ajaxPagination').fadeTo('fast',1);
					});
				}
			});
		}
		
		<?php if(!isset($rec['Whois'])):?>
			$.ajax({
				url: "<?php echo $this->Html->url('/main/get_ip_whois/'.$rec['Query']['ip']);?>.json",
				dataType: 'json',
			}).done(function( data ){
                                
                                if(typeof data.value == "undefined" || data.value == ""){data.value = "-- no data --";}
                                var header = "";
                                if(typeof data.server != "undefined"){ header = '<h5>Response from <strong>'+data.server+'<strong></h5><hr style="width:100%" />'}
				$('#ipWhoisData').html(header+data.value);
			});
		
		<?php endif; ?>
		
		
		<?php if( !empty($rec['Domain']) && (!isset($rec['Domain']['whois']) or empty($rec['Domain']['whois'])) && isset($rec['Domain']['name']) ):?>
		
			$.ajax({
				url: "<?php echo $this->Html->url('/main/get_domain_whois/'.$rec['Domain']['name']);?>/.json"
			}).done(function( data ){
                                if(typeof data.value == "undefined" || data.value == ""){data.value = "-- no data --";}
				var header = "";
				if(typeof data.server != "undefined"){ header = '<h5>Response from <strong>'+data.server+'<strong></h5><hr style="width:100%" />'}
				$('#domainWhoisData').html(header+data.value);
			});
			
		<?php endif; ?>
		
		
		<?php if(!empty($rec['Domain']) && !$rec['QueryServer']):?>
		
			$.ajax({
				url: "<?php echo $this->Html->url('/main/get_server_information/'.$rec['Query']['ip']);?>.json",
				dataType: 'json',
			}).done(function( data ){
				
				$.each( data.response, function( key, value ) {
				  $('<dt>'+key+'</dt>').appendTo('#serverInfo');
				  $('<dd>'+value+'</dd>').appendTo('#serverInfo');
				});
			});
			
		<?php endif; ?>
		
		
		<?php if(strpos($_SERVER['SERVER_NAME'], 'nomap') === false):?>

			// Where you want to render the map.
			var element = document.getElementById('map-canvas');
		
			// Height has to be set. You can do this in CSS too.
			element.style = 'height:360px;';

			// Create Leaflet map on map element.
			var map = L.map(element);

			// Add OSM tile leayer to the Leaflet map.
			L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
				attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
			}).addTo(map);

			// Target's GPS coordinates.
			var target = L.latLng('<?=$rec['Query']['geodata']['latitude']; ?>', '<?=$rec['Query']['geodata']['longitude']; ?>');

			// Set map's center to target with zoom 14.
			map.setView(target, 10);

			// Place a marker on the same location.
			L.marker(target).bindPopup('<?php echo $rec['Query']['ip']; ?>').addTo(map);
			
		<?php endif; ?>
		
		
		
		
		$.ajax({
			url: "<?php echo $this->Html->url('/main/history');?>"
		}).done(function( data ){
			$('#ajaxPagination').removeClass('text-center').html(data);
			checkAjaxPagingLinks();	
		});
		
		
		
	});	
	
	
//]]>
</script>