
<section style="margin-top:10px">
<div class="container">


<div class="row"><h2><?php echo __d('layout','Top Host Countries');?></h2></div>
<div class="row">

<?php

	foreach($countries as $country){
		echo '<div class="col-sm-3 col-sm-offset-1">';
		echo '<p>';
		echo $this->Html->image('flags/'.strtolower($country['Country']['id']).'.png');
		echo ' '.$this->Html->link($country['Country']['printable_name'],'/main/country/'.$country['Country']['id']);
		echo ' '.$this->Html->tag('span',$country['Country']['query_count'],array('class'=>'badge'));
		echo '</p>';
		echo '</div>';
	}

?>

</div>

</div>
</section>
