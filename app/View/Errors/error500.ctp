<section style="margin-top:10px">
<div class="container">
<div class="row">
<div class="">
<h2><?php echo $name; ?></h2>
<p class="error">
	<strong><?php echo __d('layout', 'Error'); ?>: </strong>
	<?php echo __d('layout', 'A fucking Internal Error Has Occurred.'); ?>
</p>
<?php
if (Configure::read('debug') > 0):
	echo $this->element('exception_stack_trace');
endif;
?>
</div></div></div></section>