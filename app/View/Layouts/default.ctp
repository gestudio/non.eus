<?php if (!isset($q)) $q='';?>
<!DOCTYPE html>
<html lang="<?php echo substr(Configure::read('Config.language'),0,2);?>">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	
	<meta property="og:title" content="IpX.pm">
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://ipx.pm">
	<meta property="og:site_name" content="IpX Ip infomation service">
	<meta property="og:description" content="<?__d('layout','IP address information lookup services');?>">
	
    <?php if(isset($noindex)): ?>
    <meta name="robots" content="noindex,nofollow">
    <?php endif;?>
	
	<meta name="description" content="<? __d('layout','Get IP address  and domain names information. Owner, location and Internet services provider for any world wide IP address.');?>">
	<meta name="keywords" content="google,reverse, dns, whois,map,maxmind,flag,flagfox,geotool,latitude,longitude,ip,location,search,ip, information,location,geolocation,address,geographical, geografica, mapa, google map, google maps, maps, map, ip, hostname, city, location, ip address, direccion ip">
	
	<title>IpX/ <?php echo $title_for_layout;?> <?php echo __d('layout','IP address information lookup services');?></title>
	
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" />
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
	<link href="//fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400,200,300,700" rel="stylesheet" type="text/css" />
	
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css(array('styles.css', 'blue.css'));
		 
		
		echo $this->fetch('meta');
		echo $this->fetch('css');
		
	?>
	
	
	

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>	
		
	<script src="https://unpkg.com/leaflet@1.0.1/dist/leaflet.js"></script>
	<link href="https://unpkg.com/leaflet@1.0.1/dist/leaflet.css" rel="stylesheet"/>
	
</head>


<body>




      <nav>
        <ul class="list-unstyled main-menu">
          
          <!--Include your navigation here-->
          <li class="text-right"><a href="index.html#" title="<?php echo __d('layout','close');?>" id="nav-close">X</a></li>
          
          <li><a href="/tools/speedtest"><?php echo __d('layout','Speed test');?> <span class="icon"></span></a></li>
          

          
          <li><a href="/main/countries"><?php echo __d('layout','Top countries');?> <span class="fa fa-document"></span></a></li>
          
          <li><a href="/tools/tos"><?php echo __d('layout','Terms of service');?> <span class="fa fa-document"></span></a></li>

          <li><?php echo $this->Html->link($this->Html->image('flags/gb.png').' English', '/eng',array('escape'=>false,'class'=>(Configure::read('Config.language')=='eng')?'selected':''));?></li>
    		  <li><?php echo $this->Html->link($this->Html->image('flags/es.png').' Español', '/esp',array('escape'=>false,'class'=>(Configure::read('Config.language')=='esp')?'selected':''));?></li>
    		  <li><?php echo $this->Html->link($this->Html->image('flags/eu.png').' Euskeraz', '/eus',array('escape'=>false,'class'=>(Configure::read('Config.language')=='eus')?'selected':''));?></li>
    		  <li><?php echo $this->Html->link($this->Html->image('flags/de.png').' Deutsch', '/deu',array('escape'=>false,'class'=>(Configure::read('Config.language')=='deu')?'selected':''));?></li>
    		  <li><?php echo $this->Html->link($this->Html->image('flags/ru.png').' Русский', '/rus',array('escape'=>false,'class'=>(Configure::read('Config.language')=='rus')?'selected':''));?></li>
    		  
		      
          <li class="text-info"><i class="fa fa-box"></i> JSON API</li>

          <li><a href="<?php echo $this->Html->url('/ip.json');?>"><?php echo __d('layout','IP or domain info');?> <span class="icon"></span></a></li>

          <li><a href="<?php echo $this->Html->url('/main/get_ip_whois/'.$_SERVER['REMOTE_ADDR'].'.json');?>"><?php echo __d('layout','IP whois');?> <span class="icon"></span></a></li>


          <li><a href="<?php echo $this->Html->url('/main/get_domain_whois/flylogs.com/.json');?>"><?php echo __d('layout','Domain whois');?> <span class="icon"></span></a></li>

          <li><a href="<?php echo $this->Html->url('/main/countries.json');?>"><?php echo __d('layout','Top countries');?> <span class="icon"></span></a></li>
		  
        </ul>
      </nav>
          
    <div class="navbar navbar-inverse navbar-fixed-top">      
        
        
        <a class="navbar-brand" title="<?__d('layout','IP address information lookup services');?>" href="/">
        	<img src="<?php echo $this->Html->url('/apple-touch-icon.png');?>" alt="ipx" width="30" /> IpX.pm <small><?php echo __d('layout','Ip information services');?></small>
        </a>
        
        <?php
        
        $q = '';
        
        if(isset($rec) && !empty($rec['Query']))
        	$q = $rec['Query']['query'];
        ?>
		
        <div class="navbar-header pull-right">
			
	        <form method="get" action="<?php echo $this->Html->url('/');?>" style="margin:1px 130px 0 0;padding:0px;" class="hidden-sm hidden-xs navbar-form navbar-left">
				<div class="form-group">
					<input type="text" style="background: none;color: white;font-size: 280%;width: 100%;margin: 0px 0 0 0;text-align: right;padding: 0px;height: 58px;line-height: 40px;" name="q" id="q" class="form-control" onblur="this.value=!this.value?'<?php echo  $q; ?>':this.value;" value="<?php echo  $q; ?>" onclick="this.value='';" />
				</div>
				<button type="submit" style="background:none;color:white;font-size:180%;border:0px;margin-top: 7px;margin-bottom:0px;" class="btn btn-default"><i class="fa fa-arrow-circle-o-right"></i></button>
			</form>
			
			<div>
	          <a id="nav-expander" class="nav-expander fixed">
	            MENU &nbsp;<i class="fa fa-bars fa-lg white"></i>
	          </a>
			</div>
        </div>
    </div>
    

		<?php echo $this->Session->flash(); ?>
		
		
		<?php echo $this->fetch('content'); ?>
			
			
				





    
    <!--Bottom-->
    <section id="bottom">
      <div class="container">
        <div class="row">
          
          <!--Section 1-->
          <div class="col-sm-3 margin-40">
            <h3>About Ipx</h3>
            <div class="gray-box">
              <p>Ipx is a free IP information tool. Get the geographical position, ISP, network and other information from any IP address or domain name.</p>
            </div>
          </div>
          
          <!--Section 2-->
          <div class="col-sm-6 margin-40">
            <h3>Email Sign Up</h3>
            <form class="row form-inline" role="form" id="email-signup">
              <div class="col-md-10">
                <input type="email" class="form-control" id="inputEmail" placeholder="Enter email">
              </div>
              <button type="submit" class="col-md-2 btn btn-primary btn-sm"><i class="fa fa-chevron-right fa-lg"></i></button>
            </form>
            <p>Sign up for the latest IPv4 and v6 news!</p>
          </div>
          
          
          <!--Section 4-->
          <div class="col-sm-3 margin-40">
            <h3>Contact</h3>
            <div class="gray-box">
              <p><i class="fa fa-phone fa-lg"></i> (+34) 674612168</p>
            </div>
            <div class="color-box">
              <p><a href="mailto:inigo@gestudio.com"><i class="fa fa-envelope-o fa-lg"></i> inigo@gestudio.com</a></p>
            </div>
            <ul class="list-inline social-icons">
              <li><a href="http://www.twitter.com/gestudio_com"><i class="fa fa-twitter"></i></a></li>
              <li><a href="https://inigo.gestudio.com"><i class="fa fa-user"></i></a></li>
              <li><a href="https://bitbucket.org/gestudio/ipx.pm"><i class="fa fa-bitbucket"></i></a></li>
            </ul>
            
          </div>
          
        </div>
      </div>
    </section>

    
    
    <!--Footer-->
    <section id="footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <p class="copyright"><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">IpX.pm</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="http://www.gestudio.com" property="cc:attributionName" rel="cc:attributionURL">Gestudio Cloud</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/deed.en_US">Creative Commons Attribution-NonCommercial 4.0 International License</a>.  Thanks to <a href="http://www.tresorg.com" title="Awesome programmer" rel="A good guy!">TresOrg</a>. <br />App <?php if(isset($version)) echo $version; ?> | Query took <?php echo round(microtime(true) - TIME_START, 3); ?> sec. | Debug levels are <?php echo Configure::read('debug');?>. | api v.<?php echo Configure::version(); ?></p>
          </div>
        </div>
      </div>
    </section>

<style></style>

<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script type="text/javascript">
	//<![CDATA[
	$(document).ready(function(){												
       
       
     //Navigation Menu Slider
      $('#nav-expander').on('click',function(e){
    		e.preventDefault();
    		$('body').toggleClass('nav-expanded');
    	});
    	$('#nav-close').on('click',function(e){
    		e.preventDefault();
    		$('body').removeClass('nav-expanded');
    	});
    	
    	
    	// Initialize navgoco with default options
      $(".main-menu").navgoco({
          caret: '<span class="caret"></span>',
          accordion: false,
          openClass: 'open',
          save: true,
          cookie: {
              name: 'navgoco',
              expires: false,
              path: '/'
          },
          slide: {
              duration: 300,
              easing: 'swing'
          }
      });
	
	    	      	
	});
	//]]>
</script>
<?php
$this->Html->script(array('classie.js','modernizr.js','nav.js'),array('inline'=>false));
echo $this->fetch('script');
echo $this->Js->writeBuffer();
echo $this->element('sql_dump');
?>


</body>
</html>