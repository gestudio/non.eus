<div class="text-center">
	<ul class="pagination">
		<?php echo $this->Paginator->prev(
			'&larr; ' . __('Previous'),
			array(
				'escape' => false,
				'tag' => 'li'
			),
			'<a onclick="return false;">&larr; '.__('Previous').'</a>',
			array(
				'class'=>'disabled prev',
				'escape' => false,
				'tag' => 'li'
			)
		);



		
		echo $this->Paginator->numbers(array('separator' => '', 'currentClass' => 'active', 'currentTag' => 'a', 'class' => '', 'tag' => 'li'));
		



		echo $this->Paginator->next(
			__('Next') . ' &rarr;',
			array(
				'escape' => false,
				'tag' => 'li'
			),
			'<a onclick="return false;">'.__('Next').' &rarr;</a>',
			array(
				'class' => 'disabled next',
				'escape' => false,
				'tag' => 'li'
			)
		);?>
	</ul>
</div>