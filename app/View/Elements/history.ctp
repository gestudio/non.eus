<div class="row">
  
  <?php
  $i = 1;
  foreach ($requests as $query):
  
  	
	echo '<div class="col-sm-5 col-sm-offset-1" title="'.date('H:m d-m-Y',$query['Query']['created']).'">';
		echo $this->Html->image('flags/'.strtolower($query['Query']['country']).'.png');
		echo $this->Html->link($query['Query']['query'], '/'.$query['Query']['query'],array('rel'=>'nofollow','class'=>'label'));
		
		echo '<small style="color:#1e2633">';
			echo $query['Query']['reverse'];
		echo '</small>';
	echo '</div>';
	
	
  endforeach;

  ?>
  
  
</div>


<?php echo $this->element('paging');?>
