<?php

class MainController extends AppController{
	
	var $uses = array('Query', 'Whois', 'QueryBlacklist', 'QueryServer');
	var $components = array('RequestHandler');
	
	
	/**
	 * beforeFilter function.
	 * 
	 * @access public
	 * @return void
	 */
	public function beforeFilter(){
		parent::beforeFilter();
		//$this->checkDNSBL();
	}
	
	
	


	public function beforeRender(){
		
		$version = $this->version();
		$this->set(compact('version'));

	}
	
	
	
	/**
	 * index function.
	 * 
	 * @access public
	 * @param mixed $query (default: null)
	 * @return void
	 */
	public function index($q = null){
		

		if(!empty($this->params['url']['q'])){
			$q = $this->params['url']['q'];
		} elseif(is_null($q) or empty($q)) {
			$q = $this->getRealIP();
		}
		

		$domain = parse_url($q);
		
		if($domain != false){
			
			if(isset($domain['host'])){
				$q = $domain = $domain['host'];
			}else{
				$q = $domain = $domain['path'];
			}
			
			try{
				$dnsa = dns_get_record($q, DNS_A);
				if(!empty($dnsa)){
					$ip = $dnsa[0]['ip'];
				}
			}catch(Exception $e){
				$ip = false;
			}
			
		}else{
			$domain = false;
		}
		
		if(!isset($ip)){
			$ip = $this->Whois->ValidateIP($q);
		} else {
			$ip = $this->Whois->ValidateIP($ip);
		}
		
		
		if($this->ip_is_private($ip)){
			throw new NotFoundException($ip . ' is a private range IP address sucker!');
		}

		if($ip == false)  {
			throw new NotFoundException(__d('layout','The provided search is not a valid IP address or a qualified domain name.'));
		}
		
		
			

			
		
		$rec = $this->Query->find('first',array(
			'conditions'=>array( 'query LIKE' => $q, 'modified >' => strtotime('-6 months') ),
			'contain' => array()
		));
		
		$geoTime = 0;

		if(empty($rec) or $this->request->isPost()):
			
			
			$rec['Query']['query']  = $q;
			$rec['Query']['ip']		= $ip;
			$rec['Query']['reverse']= gethostbyaddr($ip);
			$timeStart = microtime(true);
			


			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'http://ip-api.com/json/'.$ip);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$output = curl_exec($ch);

			$geoData = Set::reverse(json_decode($output));	

			if($output and !empty($geoData) and isset($geoData['status']) and $geoData['status'] == 'success'){


				$rec['Query']['country'] = $geoData['countryCode'];
				$rec['Query']['geodata'] = array(
					'country' 		=> $geoData['country'],
					'countryCode' 	=> $geoData['countryCode'],
					'longitude' 	=> $geoData['lon'],
					'latitude' 		=> $geoData['lat'],
					'city' 		=> $geoData['city'],
					'isp' 		=> $geoData['isp'],
					'regionName'    => $geoData['regionName'],
					'region'    => $geoData['region'],
					'zip'    => $geoData['zip'],
					'as'    => $geoData['as'],
				);
				
				
				$rec['QueryRequest']['request_ip'] = $geoData['query'];
				
				
				
			}else{
			
			
				CakeLog::write('GeoPlugin','Missing object params for geo plugin IP: '.$ip);
				
				throw new InternalErrorException($errorMsg);
			
			}
				
			$rec['Query']['modified'] = time();
				
			$this->Query->save($rec);
				
			$rec['Query']['id'] = $this->Query->id;
			
			
			$timeEnd = microtime(true);
			$geoTime = abs($timeEnd - $timeStart);
	
		endif;
		
		//$rec['Whois'] = $this->Whois->LookupIP($ip,true);
		//Take the black list query to an external ajax request since it is slowing down the process too much
		//$rec['QueryBlacklist'] = $this->QueryBlacklist->getBlackLists($ip);
		
		$wwwTime = 0;
	
		if($domain):
			$timeStart = microtime(true);
			$rec['QueryServer'] = $this->getRemoteServerInfo($ip,true);
			$rec['Domain']['name'] 	= $domain;
			$timeEnd = microtime(true);
	
			$wwwTime = $timeEnd - $timeStart;
			
			//$rec['Query']['domain']['alexa'] = $this->get_alexa_data($domain);
		endif;
		

		
		$this->Query->QueryRequest->save(array(
			'query_id' => $rec['Query']['id'],
			'request_ip' => $q
		));
		
		
		
		$totalRequests = $this->Query->find('count',array('contain'=>array()));
		

		// Html title
		$title_for_layout = $rec['Query']['query'];
		
		if(array_key_exists('city', $rec['Query']['geodata'])){
			$title_for_layout .= ' '.$rec['Query']['geodata']['city'];
		}
		
		$title_for_layout .= ' '.$rec['Query']['geodata']['country'];
		


		// Allow basic CORS from any domain for GET requests
		$this->response->header('Access-Control-Allow-Origin', '*');
		$this->response->header('Access-Control-Allow-Methods', 'GET');


		// 
		// Set final vars and render the view
		// 
		$this->set(compact('rec','totalRequests','geoTime', 'wwwTime', 'title_for_layout'));
		$this->set('_serialize',array('rec','geoTime','wwwTime'));
	
		
	}
	






	/**
	 * get_ip_whois function.
	 * 
	 * @access public
	 * @param mixed $ip (default: null)
	 * @return void
	 */
	public function get_ip_whois($ip = null){
		
		$whois = $this->Whois->LookupIP($ip,false);
		
		$this->set(compact('whois'));
		$this->set('_serialize','whois');
	}
	
	
	
	
	
	
	
	
	/**
	 * get_domain_whois function.
	 * 
	 * @access public
	 * @param mixed $domain (default: null)
	 * @return void
	 */
	public function get_domain_whois($domain = null){
		
		$whois = $this->Whois->LookupDomain($domain,false);
		

		$this->set(compact('whois'));
		$this->set('_serialize','whois');
	}
	
	
	
	
	
	
	/**
	 * history function.
	 * 
	 * @access public
	 * @return void
	 */
	public function history(){
		
		
		$this->paginate = array('order' => 'modified desc', 'limit' => 30);
		$requests= $this->paginate('Query');
		
		
		if($this->request->isAjax()){
			$this->set(compact('requests'));
			$this->layout = 'ajax';
			$this->render('/Elements/history');
		}else{
			$this->redirect('/');
		}
		
		
	}
	
	
	
	
	/**
	 * last_queries function.
	 * 
	 * @access public
	 * @return void
	 */
	public function last_queries(){
		
		$this->paginate = array(
			'order'=> 'modified desc','limit' => 10
		);
		
		$data = $this->paginate('Query');
		
		$this->set(compact('data'));
		$this->set('_serialize','data');

	}
	
	
	
	
	


	
	/**
	 * countries get all countries and rank by query numbers.
	 * 
	 * @access public
	 * @return void
	 */
	public function countries(){
		
		
		$this->loadModel('Country');
		
		$countries = $this->Country->find('all', array(
			'limit' 	=> 128,
			'contain' 	=> array(),
			'order' 	=> 'query_count desc'
		));
		
		$noindex = true;
		
		$this->set(compact('countries','noindex'));
		$this->set('_serialize','countries');

	}
	
	
	
	
	
		





	/**
	 * [country get all queries for the given country]
	 * @param  [type] $countryId [description]
	 * @return [type]            [description]
	 */
	public function country($countryId = null){


		$this->loadModel('Country');

		$country = $this->Country->find('first', array(
			'conditions' => array(
				'Country.id' => $countryId
			),
			'contain' => array()
		));

		if(is_null($countryId) || empty($country)){
			throw new NotFoundException('Country not found');
		}





		$this->paginate = array(
			'conditions' => array(
				'Query.country' => $countryId
			),
			'contain' => array(),
			'order' => 'Query.created desc'
		);

		$rows = $this->Paginate('Query');
		$noindex = true;
		
		$this->set(compact('country','rows','noindex'));
		$this->set('_serialize',array('country','rows'));

	}
	
	
	






	
	/**
	 * dig function.
	 * 
	 * @access public
	 * @return void
	 */
	public function dig(){
		
		$search = $this->params['form']['s'];
		
		if(!$this->ValidateDomain($search)){
			$result = 'Invalid domain search: '.$search;
		}else{
			$dig = '';
			exec("/usr/bin/dig " . $search, $dig);
			$result = $dig[11];
		}
		
		$this->set(compact('result'));
		$this->set('_serialize','result');
		
	}
	
	
	
	
	/**
	 * getDns function.
	 * 
	 * @access public
	 * @param mixed $domain
	 * @return void
	 */
	public function getDns($domain){
		return dns_get_record($domain);
	}
	






	/**
	 * [get_server_information description]
	 * @param  [type] $ip [description]
	 * @return [type]     [description]
	 */
	public function get_server_information($ip){
		$server = $this->getRemoteServerInfo($ip,false);

		$this->set(compact('server'));
		$this->set('_serialize','server');

	}
	
	
	
	
	/**
	 * getRemoteServerInfo function.
	 * 
	 * @access private
	 * @param mixed $query
	 * @return void
	 */
	private function getRemoteServerInfo($query, $mustBeFast = false){
	
		$response = $this->QueryServer->find('first',array(
			'conditions' => array('QueryServer.query' => $query),
			'order'	=> 'created desc',
			'contain' => array()
		));
		
		if((empty($server) or $server['QueryServer']['created'] < strtotime('-6 months')) and $mustBeFast === false):
			
			try{
				App::uses('HttpSocket', 'Network/Http');
				$HttpSocket = new HttpSocket(array('timeout' => 3));
				
				$server = $HttpSocket->get('http://'.$query, array(), array('redirect' => false));
				
				$response['QueryServer']['query'] = $query;
				$response['QueryServer']['response'] = $this->string2KeyedArray($HttpSocket->response['raw']['header'],chr(10),':');
				$response['QueryServer']['created'] = time();
				
				$this->QueryServer->create();
				$this->QueryServer->save($response);
				
				
			}catch(Exception $e){
				CakeLog::write('HttpSocket','Query to '.$query.' failed with message: '. $e->getMessage());
				return false;
			}
		
		endif;
		
		if(empty($response))
			return false;
		else
			return $response['QueryServer'];
		
		
	}

	
	
	
	
	
	/**
	 * get_alexa_data function.
	 * 
	 * @access private
	 * @param mixed $domain
	 * @return void
	 */
	private function get_alexa_data($domain){
		$url = 'http://alexa.com/xml/dad?url='.'http://'.$domain;
		$ch = curl_init();
		$timeout = 5;
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
		$xml = curl_exec($ch);
		curl_close($ch);
		return $xml;
	}














    /**
     * [version returns the version number based on number of commits pushed to the main branch]
     * @return [type] [description]
     */
    public function version() {

        exec('git describe --always', $version_mini_hash);
        exec('git rev-list HEAD | wc -l', $version_number);
        
        $vnum = 'x';

        if(isset($version_number[0])){
            $vnum = trim($version_number[0]);
        }

        $build = 'x';
        
        if(isset($version_mini_hash[0])){
            $build = trim($version_mini_hash[0]);
        }


        return "v0.".$vnum." build ".$build;

    }


		
	
	
	
}



?>