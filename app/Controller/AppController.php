<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	
	
	
	
	var $components = array('Cookie','Session');
	
	
	
	
	
	/**
	 * beforeFilter function.
	 * 
	 * @access public
	 * @return void
	 */
	public function beforeFilter(){
		
		$this->setLanguage();	
		
		if($this->request->isAjax()) $this->layout = 'ajax';
		
		$viewFolder = $this->viewPath. DS . Configure::read('Config.language');
		
		if (file_exists(APP.'View'.DS.$viewFolder)) {
			// e.g. use /app/views/fre/pages/tos.ctp instead of /app/views/pages/tos.ctp
			$this->viewPath = $viewFolder;
		}
		
		
	}
	
	
	
	
	
	
	
	/**
	 * 
	 * Function _setLanguage()
	 * 
	 * @params
	 * 
	 * @description
	 * 		Tries to read Cookie lang or Session set Language
	 * 		If does not exists, set's the cookie
	 * 
	 * @return
	 * null
	 * 
	 */
	public function setLanguage() {
		
		if(!Configure::read('Config.language')):
			$cookie = $this->Cookie->read('lang');
			$session = $this->Session->check('Config.language');
			
			if(isset($this->params['lang']) and !empty($this->params['lang'])) {
				$this->setLang($this->params['lang']);
			}elseif(!empty($cookie)){
				$this->setLang($this->Cookie->read('lang'));
			}elseif($session){
				$sessionLang = $this->Session->read('Config.language');
				$this->setLang($this->Cookie->read('lang'));
			}elseif(isset($_SERVER["HTTP_ACCEPT_LANGUAGE"])){
				if (ereg("es", $_SERVER["HTTP_ACCEPT_LANGUAGE"]))
					$this->setLang('esp');
				if (ereg("en", $_SERVER["HTTP_ACCEPT_LANGUAGE"]))
					$this->setLang('eng');
				if (ereg("de", $_SERVER["HTTP_ACCEPT_LANGUAGE"]))
					$this->setLang('deu');
				if (ereg("eu", $_SERVER["HTTP_ACCEPT_LANGUAGE"]))
					$this->setLang('eus');
				if (ereg("ru", $_SERVER["HTTP_ACCEPT_LANGUAGE"]))
					$this->setLang('rus');
			}
		endif;
	}
	
	
	
	
	
	
	
	
	/**
	 * setLang function.
	 * 
	 * @access public
	 * @param mixed $lang
	 * @return void
	 */
	public function setLang($lang){
		
		Configure::write('Config.language', $lang);
		$this->Session->write('Config.language', $lang); 
		$this->Cookie->delete('lang');
		$this->Cookie->write('lang', $lang, false, '20 days');

	}

	
	
	
	
	/**
	 * 
	 * Function getRealIp()
	 * 
	 * 
	 * @params
	 * null
	 * 
	 * @description
	 * Gets real user ip jumping over proxies and isp shadows.
	 * If can not get real ip, gets REMOTE_ADDR value.
	 * 
	 * @return
	 * $ip (string)
	 * 
	 */
	public function getRealIP(){
		
		if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
			$client_ip =( !empty($_SERVER['REMOTE_ADDR']) ) ?
				$_SERVER['REMOTE_ADDR']:
				( ( !empty($_ENV['REMOTE_ADDR']) ) ?
					$_ENV['REMOTE_ADDR']:
					false 
				);
			
			// los proxys van añadiendo al final de esta cabecera
			// las direcciones ip que van "ocultando". Para localizar la ip real
			// del usuario se comienza a mirar por el principio hasta encontrar
			// una dirección ip que no sea del rango privado. En caso de no
			// encontrarse ninguna se toma como valor el REMOTE_ADDR
			
			$entries = split('[, ]', $_SERVER['HTTP_X_FORWARDED_FOR']);
			
			reset($entries);
			
			while (list(, $entry) = each($entries)){
				$entry = trim($entry);
				if ( preg_match("/^([0-9]+.[0-9]+.[0-9]+.[0-9]+)/", $entry, $ip_list) ){
					// http://www.faqs.org/rfcs/rfc1918.html
					$private_ip = array(
					'/^0./',
					'/^127.0.0.1/',
					'/^192.168..*/',
					'/^172.((1[6-9])|(2[0-9])|(3[0-1]))..*/',
					'/^10..*/');
					
					$found_ip = preg_replace($private_ip, $client_ip, $ip_list[1]);
					
					if ($client_ip != $found_ip) {
						$client_ip = $found_ip;
						break;
					}
				}
			}
		}
		else {
			$client_ip =
			( !empty($_SERVER['REMOTE_ADDR']) ) ?
			$_SERVER['REMOTE_ADDR']
			:
			( ( !empty($_ENV['REMOTE_ADDR']) ) ?
			$_ENV['REMOTE_ADDR']
			:
			false );
		}
		
		


		return $client_ip;
		
	}
	


	/*
	 * Function checkDNSBL
	 *
	 * @description Checks current visitors IP for listings in DNS black lists 
	 * @return NULL
	 * @access public
	 *
	 */

	public function checkDNSBL() {
		$blacklists = array('web.sorbs.net');
		$parts = explode('.', $_SERVER['REMOTE_ADDR']);
		$ip = implode('.', array_reverse($parts)) . '.';
		foreach ($blacklists as $bl) {
			$check = $ip . $bl;
			if ($check != gethostbyname($check)) {
				error_log('PHP Security: [DNSBL] - ' . $_SERVER['REMOTE_ADDR'] . ' - ' . $bl);
				die('Your IP address is blocked due to it appears in SORBS.net blacklist!');
			}
		}
	}

	

	/*
	 * Function string2KeyedArray
	 * 
	 * @description converts pure string into a trimmed keyed array.
	 * @param $string (String) String with data to be cut.
	 * @param $delimiter (String) delimiter string of records.
	 * @param $kv (String) array to value delimiter string.
	 * @return $ka (array) associative array from the provided String.
	 *
	 */
	public function string2KeyedArray($string, $delimiter = ',', $kv = '=>') {
		if(empty($string)) return '';
		$ka = array();
		
		if(is_array($string))
			return false;
			
		if ($a = explode($delimiter, $string)) { // create parts
			foreach ($a as $s) { // each part
				if ($s) {
					if ($pos = strpos($s, $kv)) { // key/value delimiter
						$ka[trim(substr($s, 0, $pos))] = trim(substr($s, $pos + strlen($kv)));
					} else { // key delimiter not found
						$ka[] = trim($s);
					}
				}
			}
			return $ka;
		}
	}

	
	
	

	/**
	 * [ip_is_private return true or false wether it is or not a private CIDR ip address]
	 * @param  mixed $ip ipv4 ip address
	 * @return bool     [bool response]
	 */
	public function ip_is_private($ip){
	        $pri_addrs = array(
				'10.0.0.0|10.255.255.255',
				'172.16.0.0|172.31.255.255',
				'192.168.0.0|192.168.255.255',
				'169.254.0.0|169.254.255.255',
				'127.0.0.0|127.255.255.255'
			);

	        $long_ip = ip2long($ip);

	        if($long_ip != -1) {

	            foreach($pri_addrs AS $pri_addr){
	                list($start, $end) = explode('|', $pri_addr);

	                 // IF IS PRIVATE
	                 if($long_ip >= ip2long($start) && $long_ip <= ip2long($end))
	                 return (TRUE);
	            }
	    }

		return (FALSE);
	}
	
	
	
	
}
