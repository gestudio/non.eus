<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/main/:action/*', array('controller' => 'main'));


	Router::connect('/:lang/*', array(
		'controller' => 'main',
		'action' => 'index'
	),array('lang'=>'[a-z]{3}'));

	Router::connect('/tools/*', array('controller' => 'pages', 'action' => 'display'));

	Router::connect('/', array('controller' => 'main', 'action' => 'index'));
	Router::connect('/ip', array('controller' => 'main', 'action' => 'index'));
	Router::connect('/*', array('controller' => 'main', 'action' => 'index'));
	


/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();
	Router::parseExtensions('xml','json','cat');

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
