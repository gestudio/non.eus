<?php
App::uses('AppModel', 'Model');

class QueryServer extends AppModel{
	
	
	public $useTable = 'query_servers';
	
	
	/*
	public $belongsTo = array(
		'Query'=>array(
			'foreignKey'   => false,
			'conditions' => array('Query.query' => 'QueryServer.query')
		)
	);
	*/
	
	
	
	
	/**
	 * afterFind function.
	 * 
	 * @access public
	 * @param mixed $data
	 * @return void
	 */
	public function afterFind($data, $primary = false){
		parent::afterFind($data, $primary);

		foreach($data as &$rec){
			
			if(!empty($rec['QueryServer']['response']))
				$rec['QueryServer']['response'] = @unserialize($rec['QueryServer']['response']);
			else
				$rec['QueryServer']['response'] = array();
			
			
		}
		
		return $data;
		
	}
	
	
	
	
	
	/**
	 * beforeSave function.
	 * 
	 * @access public
	 * @param mixed $params
	 * @return void
	 */
	public function beforeSave($params = array()){
		parent::beforeSave($params);
		
		if(is_array($this->data['QueryServer']['response']))
			$this->data['QueryServer']['response'] = serialize($this->data['QueryServer']['response']);
		else 
			$this->data['QueryServer']['response'] = '';
		
		
				
		
		return true;
	}
	
	
	
}
