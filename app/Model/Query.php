<?php
App::uses('AppModel', 'Model');
class Query extends AppModel{
	
	
	public $useTable = 'queries';

	
	
	public $hasMany = array(
		'QueryRequest',
		/*'QueryBlacklist' => array(
			'foreignKey' => false,
			'conditions' => array('Query.query' => 'QueryBlacklist.query')
		),
		'QueryServer' => array(
			'foreignKey' => false,
			'conditions' => array('Query.query' => 'QueryServer.query')
		)*/
	);
	
	public $belongsTo = array(
		'Country'=>array(
			'foreignKey'   => 'country',
			'counterCache' => true
		)
	);
	
	
	/**
	 * afterFind function.
	 * 
	 * @access public
	 * @param mixed $data
	 * @return void
	 */
	public function afterFind($data, $primary = false){
		parent::afterFind($data,$primary);

		foreach($data as &$rec){
			
			if(!empty($rec['Query']['domain']))
				$rec['Query']['domain'] = @unserialize($rec['Query']['domain']);
			else
				$rec['Query']['domain'] = array();
			
			if(!empty($rec['Query']['geodata']))
				$rec['Query']['geodata'] = @unserialize($rec['Query']['geodata']);
			else
				$rec['Query']['geodata'] = array();
			
			if(!empty($rec['Query']['blacklists']))
				$rec['Query']['blacklists'] = @unserialize($rec['Query']['blacklists']);
			else
				$rec['Query']['blacklists'] = array();
			
			
		}
		
		return $data;
		
	}
	
	
	
	
	/**
	 * beforeSave function.
	 * 
	 * @access public
	 * @param mixed $params
	 * @return void
	 */
	public function beforeSave($params = array()){
		parent::beforeSave($params);
		
		if(is_array($this->data['Query']['geodata']))
			$this->data['Query']['geodata'] = serialize($this->data['Query']['geodata']);
		else 
			$this->data['Query']['geodata'] = '';
		
		
		
		return true;
	}
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	
	
	
	
		
}