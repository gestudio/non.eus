<?php
App::uses('AppModel', 'Model');

class QueryBlacklist extends AppModel{
	
	
	public $useTable = 'query_blacklists';
	
		
	protected $blackListProviders = array(
									    'zen.spamhaus.org',
									    'multi.surbl.org',
									    'multi.uribl.com'
									);
	/*
	public $belongsTo = array(
		'Query'=>array(
			'foreignKey'   => false,
			'conditions' => array('Query.query' => 'QueryBlacklist.query')
		)
	);
	*/
	
	
	
	
	/**
	 * afterFind function.
	 * 
	 * @access public
	 * @param mixed $data
	 * @return void
	 */
	public function afterFind($data, $primary = false){
		parent::afterFind($data, $primary);

		foreach($data as &$rec){
			
			if(!empty($rec['QueryBlacklist']['response']))
				$rec['QueryBlacklist']['response'] = @unserialize($rec['QueryBlacklist']['response']);
			else
				$rec['QueryBlacklist']['response'] = array();
			
			
		}
		
		return $data;
		
	}
	
	
	
	
	
	/**
	 * beforeSave function.
	 * 
	 * @access public
	 * @param mixed $params
	 * @return void
	 */
	public function beforeSave($params = array()){
		parent::beforeSave($params);
		
		if(is_array($this->data['QueryBlacklist']['response']))
			$this->data['QueryBlacklist']['response'] = serialize($this->data['QueryBlacklist']['response']);
		else 
			$this->data['QueryBlacklist']['response'] = '';
		
		
				
		
		return true;
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Check a URL against the 3 major blacklists
	 *
	 * @param string $url The URL to check
	 * @return mixed true if blacklisted, false if not blacklisted, 'malformed' if URL looks weird
	 */

	/**
	 * is_blacklisted function.
	 * 
	 * @access public
	 * @param mixed $ip
	 * @return void
	 */
	public function getBlackLists($ip) {
	
		$prevRec = $this->find('first',array(
			'conditions' => array('QueryBlacklist.query'=>$ip),
			'order' => 'created desc',
			'contain' => array()
		));
		
		if(!empty($prevRec) and $prevRec['QueryBlacklist']['created'] < strtotime('-6 months')):
			return $prevRec['QueryBlacklist'];
			
		else:
			
			$blacklists = $this->blackListProviders;
			$result = array();
			
			foreach($blacklists as $blacklist) {
				$url = $this->buildurl($ip, $blacklist);
				$record = dns_get_record($url);
				if ($record === "127.0.0.2")
					$result[$blacklist] =  true;
				else
					$result[$blacklist] =  false;
			}
			
			$data = array('query' => $ip, 'response' => $result, 'created' => time());
			
			$this->create();
			$this->save($data);
			
			return $data;
			
		 endif;
		 
		 
		 
	}
	
		
	
	/**
	 * buildurl function.
	 * 
	 * @access private
	 * @param mixed $ip
	 * @param mixed $blacklistprovider
	 * @return void
	 */
	private function buildurl($ip, $blacklistprovider) {
	  return implode(".", array_reverse(explode(".", $ip))) . $blacklistprovider;
	}
	
}
